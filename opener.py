"""

"""

import operator

with open("encrypted.txt", "r") as file: # read char frequency in encrypted message
    charfreq = {} # empty dict
    total = 0
    while (True):
        c = file.read(1)
        
        if c == '': # EOF
            break
        if c == ' ': # Don't count whitespace
            continue
        total = total + 1 # count total for percentages
        if c not in charfreq: # 
            charfreq[c] = 1
        else:
            charfreq[c] = charfreq[c] + 1

sorted_charfreq = sorted(charfreq.items(), key=operator.itemgetter(1), reverse=True) # transform into sorted list of tuples
sorted_charfreqlist = []
for elem in sorted_charfreq:
    sorted_charfreqlist.append((elem[0],round(elem[1]/total*100,2))) # transform into percentages

print("Char frequency in encrypted.txt:")
for elem in sorted_charfreq:
    print(elem)

with open("finnish_monograms.txt", "r") as file: # read char frequency in Finnish
    finfreq = {}
    totalfin = 0
    while (True):
        line = file.readline()
        if line == '': # EOF
            break
        letter, count = line.split(' ') # should be char and int
        totalfin = totalfin + int(count) # count total for percentages
        finfreq[letter] = int(count)
        
sorted_finfreq = sorted(finfreq.items(), key=operator.itemgetter(1), reverse=True) # transform into sorted list of tuples
sorted_finfreqlist = []
for elem in sorted_finfreq:
    sorted_finfreqlist.append((elem[0],round(elem[1]/totalfin*100,2))) # transform into percentages

print("------------------------------------------------------------------------------------")
print("Char frequency in finnish_monograms.txt:")
for elem in sorted_finfreqlist:
    print(elem)

with open("encrypted.txt", "r") as file: # read word frequency in encrypted message
    wordfreq = {}
    totalwords = 0
    string = file.readline() # just one line
    words = string.split(' ') # split the line into words
    for word in words:
        totalwords = totalwords + 1 # count total for percentages
        if word in wordfreq:
            wordfreq[word] = wordfreq[word] + 1
        else:
            wordfreq[word] = 1

sorted_words = sorted(wordfreq.items(), key=operator.itemgetter(1), reverse=True) # transform into sorted list of tuples
sorted_wordslist = []
for elem in sorted_words:
    sorted_wordslist.append((elem[0],round(elem[1]/totalwords*100,2))) # transform into percentages

print("------------------------------------------------------------------------------------")
print("Word frequency in encrypted.txt:")
for elem in sorted_wordslist:
    print(elem)

substdict = {}
substdict["T"] = "J"
substdict["G"] = "A"
substdict["K"] = "O"
substdict["H"] = "N"
substdict["P"] = "K"
substdict["Ö"] = "Ä"
substdict["Q"] = "V"
substdict["O"] = "T"
substdict["V"] = "I"
substdict["Y"] = "E"
substdict["E"] = "L"
substdict["U"] = "M"
substdict["Z"] = "U"
substdict["M"] = "S"
substdict["S"] = "Y"
substdict["J"] = "R"
substdict["I"] = "P"
substdict["L"] = "G"
substdict["A"] = "H"
substdict["X"] = "D"

print("------------------------------------------------------------------------------------")
print("Original encrypted.txt:")
with open("encrypted.txt", "r") as file:
    while (True):
        c = file.read(1)
        if c == '':
            break
        elif c == ' ':
            print (' ', end = '')
        else:
            print(c, end='')

print("\n------------------------------------------------------------------------------------")
print("Decrypted text:")
with open("encrypted.txt", "r") as infile:
    with open("decrypted.txt", "w+") as outfile:
        while (True):
            c = infile.read(1)
            if c == '':
                break
            elif c == ' ':
                print (' ', end = '') # print space
                outfile.write(' ') # into file
            elif c in substdict:
                print(substdict[c], end = '') # substitute the letters from substdict
                outfile.write(substdict[c]) # into file
            else:
                print(c, end='') # if char not found in substitution dict
                outfile.write(c) # into file
print("") # newline to the end
